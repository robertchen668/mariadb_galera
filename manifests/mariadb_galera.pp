#Author: Robert Chen, Sep, 2015

class mariadb_galera::mariadb_galera (

  $my_cnf_conf = "mariadb_galera/my.cnf.erb",
  $wsrep_sst_method = hiera('wsrep_sst_method'),
  $wsrep_cluster_address = hiera('wsrep_cluster_address'),
  $galera_cluster_name  = hiera('galera_cluster_name'),

  # Ubuntu/mariadb
#  $apt_mariadb_repo_location = 'http://ftp.osuosl.org/pub/mariadb/repo/10.0/ubuntu',
#  $apt_mariadb_repo_release = $::lsbdistcodename,
#  $apt_mariadb_repo_repos = 'main',
#  $apt_mariadb_repo_include_src = false,
) {
  
  exec {'add-mariadbkey':
    unless => '/usr/bin/apt-key list|grep -i MariaDB',
    command => '/usr/bin/apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db',
    notify => Exec['add-mariadbrepo'],
  }
  
  exec {'add-mariadbrepo': 
    unless => '/bin/cat /etc/apt/sources.list | grep mariadb',
    command => "/usr/bin/add-apt-repository 'deb http://ftp.osuosl.org/pub/mariadb/repo/10.0/ubuntu trusty main'",
    notify => Exec['apt-get-update'],
  }
  
#  include apt

# too bad, this does not work, hkp bug? 
#  apt::key { 'mariadbkey':
#    key => '0xcbcb082a1bb943db',
#    source => 'hkp://keyserver.ubuntu.com:80',
#  }

#  apt::source { 'galera_mariadb_repo':
#    ensure => present,
#    location          => $apt_mariadb_repo_location,
#    release           => $apt_mariadb_repo_release,
#    repos             => $apt_mariadb_repo_repos,
#    include_src       => $apt_mariadb_repo_include_src,
#    require => Apt::Key['mariadbkey'],
#  }

  exec { 'apt-get-update':  
    command     => '/usr/bin/apt-get update',
    refreshonly => true,
  }

  package { [ "software-properties-common", "mariadb-galera-server" ]:
    ensure => installed,
    require    => [ Exec['apt-get-update'], Exec["add-mariadbrepo"] ]
  }

  file { "/etc/mysql/my.cnf":
      ensure  => file,
#    notify  => Service[mysql],
      content => template($my_cnf_conf),
      require => Package['mariadb-galera-server'],
  }


}
